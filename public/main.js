let helpHidden = true;

document.addEventListener('DOMContentLoaded', function() {
  setVolume();
  run();
});

const data = {
  // This is the main data, by composer last name
  mrowiec1: {
    color: 'magenta',
    fillColor: 'magenta',
    info: 'Ks. Mrowiec (ver 1)',
    locations: {
      'Siedlce': {
        coord: [52.170,22.271],
        info: `
          Parafia pw. Niepokalanego Poczęcia NMP W Siedlcach<br
          ul. J. Kochanowskiego 11, 08-110 Siedlce<br>
          ${link('http://katedra.siedlce.pl/')}<br>
          ${ytlink('a2cHifHYmvY?start=1877')}
        `
      },
      'Krakow, Swoszowice': {
        coord: [49.994, 19.931],
        info: `
          Parafia Opatrzności Bożej Kraków Swoszowice
          ul. Kąpielowa 61, 30-698 Kraków<br>
          ${ytlink('h_9fWOjICwY?start=2709')}
        `
      },
    },
  },
  pawlak: {
    color: 'grey',
    fillColor: 'grey',
    info: 'Ks. Pawlak',
    locations: {
      'Krakow, Kazimierz': {
        coord: [50.049, 19.939],
        info: `
          ${link('http://parafia-kazimierz.augustianie.pl/vtour/')}<br>
          Parafia p.w. Św. Katarzyny Aleksandryjskiej<br>
          ul. Augustiańska 7, 31-064 Kraków<br>
          ${ytlink('YsuRTmABObg?start=4086')}
        `
      },
      'Krakow, Swoszowice': {
        coord: [49.994, 19.931],
        info: `
          Parafia Opatrzności Bożej Kraków Swoszowice
          ul. Kąpielowa 61, 30-698 Kraków<br>
          ${ytlink('h_9fWOjICwY?start=2709')}
        `
      },
      'Łódź': {
        coord: [51.7486766,19.4588035],
        info: `
          Bazylika archikatedralna św. Stanisława Kostki
          ul. Piotrkowska 265, 90-457 Łódź
          ${ytlink('myDpuf69T9w?start=4245')}
        `
      },
      'Wrocław': {
        coord: [51.1067914, 17.1054692],
        info: `
          Parafia pw. Świętej Rodziny<br>
          ul. Monte Cassino 68, 51-681 Wrocław, Poland<br>
          ${link('http://www.sw.rodzina.archidiecezja.wroc.pl/')}<br>
          ${ytlink('YI6-wH8qfmQ?start=2490')}
        `
      },
    },
  },
  piasecki: {
    color: 'blue',
    fillColor: 'blue',
    info: 'Ks. Piasecki',
    locations: {
      'Lublin': {
        coord: [51.2467664, 22.5661802],
        info: `
          Archikatedra św. Jana Chrzciciela i św. Jana Ewangelisty
          ul. Królewska 10, 20-109 Lublin
          ${ytlink('Agt0_40P3AY?start=1900')}
        `
      },
      'Niepokalanow': {
        coord: [52.21, 20.41],
        info: 'abc',
      },
      'Wrocław': {
        coord: [51.1042, 17.1436],
        info: `
          Parafia NMP Bolesnej
          ul. Tatarakowa 1, 51-516 Wrocław
          ${ytlink('2wObvPgOVN4?start=2643')}
        `
      },
    },
  },
  rak: {
    color: 'green',
    info: 'Ks R. Rak',
    locations: {
      Opole: {
        coord: [50.67, 17.90],
        info: `${ytlink('DNn37LxCsk0')}`,
      },
    },
  },
  scibor: {
    color: 'red',
    fillColor: 'red',
    info: 'Ks. Ścibor, Józef',
    locations: {
      Mielec: {
        coord: [50.30, 21.44],
      },
    }
  },
};


function setVolume() {
  let players = document.getElementsByClassName('player');
  console.log(players);
  for(let player of players) {
    player.volume = 0.2;
  }
}


function ytlink(url) {
  return `<iframe src='https://www.youtube.com/embed/${url}' title='YouTube video player' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>`;
}

function link(url) {
  return `<a href='${url}'>${url}</a>`;
}

function run() {
  //var map = L.map('map').setView([51.505, -0.09], 13);

  const url = 'https://cdn.rawgit.com/johan/world.geo.json/34c96bba/countries/POL.geo.json';
  fetch(url)
    .then(res => res.json())
    .then((geoJSON) => {
      // Draw map
      var map = L.map('map');
      var osm = new L.TileLayer.BoundaryCanvas("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        boundary: geoJSON,
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, UK shape <a href="https://github.com/johan/world.geo.json">johan/word.geo.json</a>'
      });
      map.addLayer(osm);
      var layer = L.geoJSON(geoJSON);
      map.fitBounds(layer.getBounds());

      // Draw data
      for (composer in data) {
        console.log(composer); // DEBUG
        let composerData = data[composer]
        //let msg = composerData.info;
        for (city in data[composer]['locations']) {
          let msg = `<strong>${city}</strong><br>
            ${composerData.info}
          `;
          let cityData = data[composer].locations[city];
          let circle = L.circle(cityData.coord, {
              color: composerData.color,
              fillColor: composerData.fillColor,
              fillOpacity: 0.5,
              radius: 5000,
          }).addTo(map);
          if (cityData.hasOwnProperty('info')) {
            msg = msg + '<br>' + cityData.info;
          }
          circle.bindPopup(msg);
        };
      };
    })
}

function showHideHelp() {
  const help = document.getElementById('help');
  helpHidden = !helpHidden;
  help.style.display = helpHidden? 'none' : 'block';
  const btnHelp = document.getElementById('btnHelp');
  btnHelp.style.display = helpHidden? 'block' : 'none';
}
